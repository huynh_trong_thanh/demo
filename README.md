## DEMO LARAVEL UPLOAD FILE
    Test upload file

## Installation
- Clone or download project at: https://gitlab.com/huynh_trong_thanh/demo.git
- Install PHP and COMPOSER
- Install database Postgres  

## Basic usage
* In project create file **.env**:

    APP_NAME=Laravel
    APP_ENV=local
    APP_KEY=base64:4OITNy8t+eAbwzK+TZ6SFWwefMqnCf5wD/KQU7ZU260=
    APP_DEBUG=true
    APP_URL=http://localhost
    
    LOG_CHANNEL=stack
    
    DB_CONNECTION=pgsql
    DB_HOST=127.0.0.1
    DB_PORT=5432
    DB_DATABASE=postgres
    DB_USERNAME=postgres
    DB_PASSWORD=your password
    
    BROADCAST_DRIVER=log
    CACHE_DRIVER=file
    QUEUE_CONNECTION=sync
    SESSION_DRIVER=cookie
    SESSION_LIFETIME=120
    
    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379
    
    MAIL_DRIVER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS=null
    MAIL_FROM_NAME="${APP_NAME}"
    
    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_DEFAULT_REGION=us-east-1
    AWS_BUCKET=
    
    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_APP_CLUSTER=mt1
    
    MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
* Run Postgres: open pgAdmin 4 window..
    - Create database with config in file **.env**:
        DB_CONNECTION=pgsql
        DB_HOST=127.0.0.1
        DB_PORT=5432
        DB_DATABASE=postgres
        DB_USERNAME=postgres
        DB_PASSWORD=your password
    - Create table "module_file" with 3 columns:
        id, name, link: type text
    (https://gitlab.com/huynh_trong_thanh/demo/blob/master/public/database.png)
* Opern terminal:
    - cd project
    - run: php artisan key:generate
    - run: php artisan serve
* Open Chorme:
    - run: https://127.0.0.1:8000/index
    

## Contributing
Pull requests welcome with bug fixes, documentation improvements, and enhancements.

When making big changes, please open an issue first to discuss.

## License
This project is licensed under the MIT License.