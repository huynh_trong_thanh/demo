<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js"></script>
    </head>
    <body>
        <div>
            <input type="file" id="fileTest" required="true">
            <br/>
            <input type="submit" id="submit">
        </div>
        <div style="margin-top:20px">
            <p>List file:</p>
            <div id="list-file">
            </div>
        </div>
    </body>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function makeid(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        function getfile(){
            $.ajax({
                url : "http://127.0.0.1:8000/get-file", // gửi ajax đến file result.php
                type : "get", // chọn phương thức gửi là get
                dateType: "json", // dữ liệu trả về dạng text
                data : {},
                success : function (result){
                    $("#list-file").empty();
                   for(i=0;i<result.length;i++){
                        $("#list-file").append(
                            '<div style="margin-top:30px; border-bottom: 1px dotted black">'+
                                '<p style="color:black"><b>id:</b> <span>'+result[i].id+'</span></p>'+
                                '<p style="color:black"><b>name:</b> <span>'+result[i].name+'</span></p>'+
                                '<p style="color:black"><b>link:</b> <span>'+result[i].link+'</span></p>'+
                            '</div>'
                        )
                   }
                }
            });
        }

        getfile();

        function writefile(url){
            $.ajax({
                url : "http://127.0.0.1:8000/write-file", // gửi ajax đến file result.php
                type : "get", // chọn phương thức gửi là get
                dateType: "json", // dữ liệu trả về dạng text
                data : { // Danh sách các thuộc tính sẽ gửi đi
                    id: makeid(5),
                    name: 'test_'+makeid(5),
                    link: 'http://127.0.0.1:8000'+url 
                },
                success : function (result){
                    alert(result);
                    getfile();
                }
            });
        }

        function uploadfile(form_data){
            $.ajax({
                url : "http://127.0.0.1:8000/upload-file", // gửi ajax đến file result.php
                type : "post", // chọn phương thức gửi là get
                contentType:false,
                data : form_data,
                cache : false,
                processData: false,
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                success : function (result){
                    result = result.replace(".","");
                    writefile(result)
                }
            });
        }

        
        $("#submit").click(function(){
            var file_data = $('#fileTest').prop('files')[0];
            var form_data = new FormData();
            form_data.append('fileTest', file_data);
            uploadfile(form_data)
        });

    </script>
</html>