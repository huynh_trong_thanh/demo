<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');    
});

//api
Route::get('get-file','Controller@get_file');
Route::get('write-file','Controller@write_file');
Route::post('upload-file','Controller@upload_file');

Route::get('delete-file',function(){
    DB::table('module_file')->delete();
});


//view
Route::get('/index', function () {
    return view('index');    
});


