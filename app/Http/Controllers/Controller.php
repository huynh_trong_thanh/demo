<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    function get_file(){
        $data = DB::select("
        select * from module_file
        ");
        return $data;
    }

    function write_file(Request $request){
        $id = $request->id;
        $name = $request->name;
        $link = $request->link;
        
        $data = DB::table("module_file")->insert([
            "id" => $id,
            "name" => $name,
            "link" => $link
        ]);
        return "Cập nhật thành công!";
    }

    public function upload_file(Request $request)
    {
        //Kiểm tra file
        if ($request->hasFile('fileTest')) {
            $file = $request->fileTest;
            $link = $file->move('./upload', $file->getClientOriginalName());
            return $link;
        }
    }

    

}
